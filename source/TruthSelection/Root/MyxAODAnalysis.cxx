#include <TruthSelection/MyxAODAnalysis.h>

#include <xAODJet/JetContainer.h>
#include "xAODTracking/VertexContainer.h"

#include "xAODTracking/TrackParticlexAODHelpers.h"
#include "xAODTruth/TruthParticleContainer.h"

#include "xAODBTagging/BTagging.h"
#include "xAODBTagging/BTaggingContainer.h"

#include "xAODEventInfo/EventInfo.h" 
#include "TMVA/Reader.h"

#include "xAODJet/JetContainer.h" 
#include "xAODMissingET/MissingETContainer.h" 
#include "xAODMissingET/MissingETAuxContainer.h" 
#include "xAODMissingET/MissingETComposition.h"

using namespace std;

MyxAODAnalysis :: MyxAODAnalysis (const std::string& name,
                                  ISvcLocator *pSvcLocator)
    : EL::AnaAlgorithm (name, pSvcLocator)
{
  // Here you put any code for the base initialization of variables,
  // e.g. initialize all pointers to 0. Note that things like resetting
  // statistics variables should rather go into the initialize() function.
}

StatusCode MyxAODAnalysis :: initialize (){
  
  m_eventCounter = 0;
  book (TH1F ("Higgs_PT", "", 50, 0, 1000));
  book (TH1F ("WBoson_PT", "", 50, 0, 1000));
  book (TH1F ("TopInProduction_PT", "", 50, 0, 600));
  book (TH1F ("TopInProduction_Eta", "", 50, -5, 5));
  book (TH1F ("BottomInProduction_PT", "", 50, 0, 300));
  book (TH1F ("BottomInProduction_Eta", "", 50, -5, 5));
  book (TH1F ("NJets", "", 13, -0.5 , 12.5));
  book (TH1F ("NBJets", "", 7, -0.5 , 6.5));
  book (TH1F ("NBBJets", "", 4, -0.5 , 3.5));

  book (TH1F ("BJet1_PT", "", 50, 0, 500));
  book (TH1F ("BJet2_PT", "", 50, 0, 500));
  book (TH1F ("BJet3_PT", "", 50, 0, 500));
  book (TH1F ("BJet4_PT", "", 50, 0, 500));

  book (TH1F ("BJet1_Eta", "", 50, -5, 5));
  book (TH1F ("BJet2_Eta", "", 50, -5, 5));
  book (TH1F ("BJet3_Eta", "", 50, -5, 5));
  book (TH1F ("BJet4_Eta", "", 50, -5, 5));

  book (TH1F ("mB1B2", "", 50, 0, 500));
  book (TH1F ("mB1B3", "", 50, 0, 500));
  book (TH1F ("mB2B3", "", 50, 0, 500));
  book (TH1F ("mB1B4", "", 50, 0, 500));
  book (TH1F ("mB2B4", "", 50, 0, 500));
  book (TH1F ("mB3B4", "", 50, 0, 500));

  return StatusCode::SUCCESS;
}

StatusCode MyxAODAnalysis :: execute (){
      return Reconstruct_ChargedHiggs();
}

StatusCode MyxAODAnalysis :: Reconstruct_ChargedHiggs(){

  const xAOD::EventInfo* eventInfo = 0;
  ANA_CHECK(evtStore()->retrieve( eventInfo, "EventInfo"));
  const std::vector<float> mc_event_weights=eventInfo->mcEventWeights();
  float default_weight = eventInfo->mcEventWeight();

  TLorentzVector MET; MET.SetPtEtaPhiM(0.,0.,0.,0.);
  TLorentzVector Neutrino; Neutrino.SetPtEtaPhiM(0.,0.,0.,0.);
  const xAOD::MissingETContainer *METCont=0;
  ANA_CHECK(evtStore()->retrieve(METCont , "MET_Truth"));
  float met_truth = METCont->at(0)->met()*0.001;
  float MET_truth_phi=METCont->at(0)->phi();
  MET.SetPtEtaPhiM(met_truth,0.,MET_truth_phi,0.);

  bool found_b = false;
  TLorentzVector WBoson, Higgs, TopQ_InProduction, BottomQ_InProduction;
  const xAOD::TruthParticleContainer* truthParticles = 0;
  ANA_CHECK(evtStore()->retrieve(truthParticles, "TruthParticles"));
  for (auto TruthParticle : *truthParticles) {
      /////std::cout<< TruthParticle->pdgId()<<"   "<<TruthParticle->status()<<std::endl;      
      if(TruthParticle->pdgId()==25 &&  TruthParticle->status() == 22){
            Higgs = TruthParticle->p4();
      }
      if(fabs(TruthParticle->pdgId())==24){
            WBoson = TruthParticle->p4();     
      }
      if(fabs(TruthParticle->pdgId()) == 5 && TruthParticle->status()== 62 && found_b == false){
          BottomQ_InProduction = TruthParticle->p4();
          found_b              = true;
      }
      if(fabs(TruthParticle->pdgId()) == 6 && TruthParticle->status()== 62 ){
          TopQ_InProduction    = TruthParticle->p4();
      }
  }

  unsigned int nLeptons = 0;
  std::vector<TLorentzVector> Leptons;
  TLorentzVector Lep_dressed;
  const xAOD::TruthParticleContainer* electrons = 0;
  ANA_CHECK(evtStore()->retrieve( electrons,"TruthElectrons"));
  for (auto electron : *electrons) {
      int status=electron->status();
      if(fabs(electron->auxdata<int>("motherID")) == 24)nLeptons++;
      if(fabs(electron->auxdata<int>("motherID")) != 24 && fabs(electron->auxdata<int>("motherID")) != 15)continue;
      ///if(status!=1)continue;
      ///if(fabs(electron->auxdata<float>("eta_dressed"))>2.47)continue;
      ///if(fabs(electron->auxdata<float>("eta_dressed"))>1.37 && fabs(electron->auxdata<float>("eta_dressed"))<1.52)continue;
      ///if(electron->auxdata<float>("pt_dressed")<27e3)continue;
      //std::cout<<electron->auxdata<int>("motherID")<<std::endl;
      Lep_dressed.SetPtEtaPhiE(electron->auxdata<float>("pt_dressed"),electron->auxdata<float>("eta_dressed"),electron->auxdata<float>("phi_dressed"),electron->auxdata<float>("e_dressed"));
      Leptons.push_back(Lep_dressed);  
  }

  const xAOD::TruthParticleContainer* muons = 0;
  ANA_CHECK(evtStore()->retrieve(muons,"TruthMuons"));
  for (auto muon : *muons) {
      int status=muon->status();
      if(fabs(muon->auxdata<int>("motherID")) == 24)nLeptons++;
      if(fabs(muon->auxdata<int>("motherID")) != 24 && fabs(muon->auxdata<int>("motherID")) != 15)continue;
      ///if(status!=1)continue;
      ///if(fabs(muon->auxdata<float>("eta_dressed"))>2.5)continue;
      ///if(muon->auxdata<float>("pt_dressed")<27e3)continue;
      ///std::cout<<muon->auxdata<int>("motherID")<<std::endl;
      Lep_dressed.SetPtEtaPhiE(muon->auxdata<float>("pt_dressed"),muon->auxdata<float>("eta_dressed"),muon->auxdata<float>("phi_dressed"),muon->auxdata<float>("e_dressed"));
      Leptons.push_back(Lep_dressed);
  }

  const xAOD::TruthParticleContainer* taus = 0;
  ANA_CHECK(evtStore()->retrieve(taus,"TruthTaus"));
  for (auto tau : *taus) {
      if(fabs(tau->pdgId()) == 15 && (tau->auxdata<unsigned int>("classifierParticleOrigin") == 10 || tau->auxdata<unsigned int>("classifierParticleOrigin") ==12)){
               nLeptons++;
      }
  }
  if(nLeptons != 1) return StatusCode::SUCCESS;

  hist("Higgs_PT")->Fill(Higgs.Pt()*0.001,default_weight);
  hist("WBoson_PT")->Fill(WBoson.Pt()*0.001,default_weight);
  hist("TopInProduction_PT")->Fill(TopQ_InProduction.Pt()*0.001,default_weight);
  hist("TopInProduction_Eta")->Fill(TopQ_InProduction.Eta(),default_weight);
  hist("BottomInProduction_PT")->Fill(BottomQ_InProduction.Pt()*0.001,default_weight);
  hist("BottomInProduction_Eta")->Fill(BottomQ_InProduction.Eta(),default_weight);

  int NJets(0),NBJets(0),NBBJets(0); 
  TLorentzVector Jet1, Jet2;
  std::vector<TLorentzVector> BJets; 
  const xAOD::JetContainer* jets = 0;
  ANA_CHECK(evtStore()->retrieve( jets, "AntiKt4TruthJets" ));
  for (auto jet : *jets) {
      if(jet->pt()*0.001 < 20. || std::fabs(jet->eta()) > 4.5)continue;
      if(std::fabs(jet->eta()) > 2.5 && jet->pt()*0.001 < 30.)continue;
      bool removeJet = false;
      for(auto lep : Leptons){
         if(jet->p4().DeltaR(lep) < 0.3)removeJet=true;
      }
      if(removeJet==true)continue;
      NJets++;
      if(jet->auxdata<int>("HadronConeExclTruthLabelID") == 5){
          NBJets++;
          BJets.push_back(jet->p4());
      }
      if(jet->auxdata<int>("GhostBHadronsFinalCount") > 1)NBBJets++;
  }
  hist("NJets")->Fill(NJets,default_weight);
  hist("NBJets")->Fill(NBJets,default_weight);
  hist("NBBJets")->Fill(NBBJets,default_weight);
  if(BJets.size() > 0){
       hist("BJet1_PT")->Fill(BJets.at(0).Pt()*0.001,default_weight);
       hist("BJet1_Eta")->Fill(BJets.at(0).Eta(),default_weight);
  }
  if(BJets.size() > 1){
       hist("BJet2_PT")->Fill(BJets.at(1).Pt()*0.001,default_weight);
       hist("BJet2_Eta")->Fill(BJets.at(1).Eta(),default_weight);

       hist("mB1B2")->Fill((BJets.at(0)+BJets.at(1)).M()*0.001,default_weight);
  }
  if(BJets.size() > 2){
       hist("BJet3_PT")->Fill(BJets.at(2).Pt()*0.001,default_weight);
       hist("BJet3_Eta")->Fill(BJets.at(2).Eta(),default_weight);
       
       hist("mB1B3")->Fill((BJets.at(0)+BJets.at(2)).M()*0.001,default_weight);
       hist("mB2B3")->Fill((BJets.at(1)+BJets.at(2)).M()*0.001,default_weight);
  }
  if(BJets.size() > 3){
       hist("BJet4_PT")->Fill(BJets.at(3).Pt()*0.001,default_weight);
       hist("BJet4_Eta")->Fill(BJets.at(3).Eta(),default_weight);

       hist("mB1B4")->Fill((BJets.at(0)+BJets.at(3)).M()*0.001,default_weight);
       hist("mB2B4")->Fill((BJets.at(1)+BJets.at(3)).M()*0.001,default_weight);
       hist("mB3B4")->Fill((BJets.at(2)+BJets.at(3)).M()*0.001,default_weight);
  }


  return StatusCode::SUCCESS;
}

StatusCode MyxAODAnalysis :: finalize ()
{
  cout << "End. We have processed " << m_eventCounter << " events" << endl;
  return StatusCode::SUCCESS;
}


void MyxAODAnalysis::getDecayProducts(const xAOD::TruthParticle* Particle){
   const std::vector< ElementLink<xAOD::TruthParticleContainer> > DecayProducts = Particle->auxdata< std::vector<ElementLink<xAOD::TruthParticleContainer> > >("childLinks");
   for(unsigned int k=0; k< DecayProducts.size(); ++k){
        if( ! (DecayProducts[k]).isValid() ) continue;
        const xAOD::TruthParticle* Child = *(DecayProducts[k]);
        if( ! Child ) continue;
        int childID     = Child->pdgId();
        int childStatus = Child->status();
        std::cout<<  childID <<"  "<<childStatus <<std::endl;
   }
  /*const xAOD::TruthParticleContainer* truthBosons = 0;
  ANA_CHECK(evtStore()->retrieve(truthBosons, "TruthBoson"));
  for (auto truthBoson : *truthBosons) {
      std::cout<< truthBoson->pdgId()<<"   "<<truthBoson->status()<<"   "<<std::endl;
      /*const std::vector< ElementLink<xAOD::TruthParticleContainer> > ParentParticle = truthBoson->auxdata< std::vector<ElementLink<xAOD::TruthParticleContainer> > >("parentLinks");
      if(fabs((*ParentParticle[0])->pdgId()) == 37){
           std::cout<< truthBoson->pdgId()<<"   "<<truthBoson->status()<<"   "<<(*ParentParticle[0])->pdgId()<<"   "<<std::endl;
           getDecayProducts(truthBoson);
      }else if (fabs((*ParentParticle[0])->pdgId()) == 6){
           std::cout<< truthBoson->pdgId()<<"   "<<truthBoson->status()<<"   "<<(*ParentParticle[0])->pdgId()<<"   "<<std::endl;
           getDecayProducts(truthBoson);
      }else{
           std::cout<< truthBoson->pdgId()<<"   "<<truthBoson->status()<<"   "<<(*ParentParticle[0])->pdgId()<<"   "<<std::endl;
           getDecayProducts(truthBoson);
      }
  }*/
}

void MyxAODAnalysis::getNeutrinoPz (double MET, double MET_phi, const TLorentzVector &lepVec, double &nu_pz1, double &nu_pz2) {
  // This code gives results identical (except using a slightly different mW) to:
  // https://svnweb.cern.ch/trac/atlasphys-exa/browser/Physics/Exotic/Analysis/DibosonResonance/
  // Data2015/VV_JJ/Code/trunk/CxAODReader_DB/Root/AnalysisReader_DB.cxx?rev=239154#L812
  double mW = 80385.; // PDG 2014

  double el  = lepVec.E();
  double ptl = lepVec.Pt();
  double pzl = lepVec.Pz();

  TLorentzVector metVec;

  metVec.SetPtEtaPhiM(MET, 0, MET_phi, 0);

  double mu    = 0.5 * mW * mW + ptl * MET * cos(lepVec.DeltaPhi(metVec));
  double delta = (mu * mu * pzl * pzl / (pow(ptl, 4))) - (el * el * MET * MET - mu * mu) / (ptl * ptl);

  if (delta < 0) {
    delta = 0;
  }

  nu_pz1 = (mu * pzl / (ptl * ptl)) + sqrt(delta);
  nu_pz2 = (mu * pzl / (ptl * ptl)) - sqrt(delta);
} // getNeutrinoPz


double MyxAODAnalysis::getNeutrinoPz (double MET, double MET_phi, const TLorentzVector &lepVec, bool min) {
  double nu_pz1;
  double nu_pz2;
  getNeutrinoPz(MET, MET_phi, lepVec, nu_pz1, nu_pz2);
  if (fabs(nu_pz1) < fabs(nu_pz2)) {
    if (min) return nu_pz1;

    return nu_pz2;
  }
  if (min) return nu_pz2;
  return nu_pz1;
}


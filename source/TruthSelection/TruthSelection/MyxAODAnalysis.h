#ifndef MyAnalysis_MyxAODAnalysis_H
#define MyAnalysis_MyxAODAnalysis_H

#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/TEvent.h"
#include "AnaAlgorithm/AnaAlgorithm.h"
///#include "SystematicsHandles/SysReadHandle.h"
///#include "SystematicsHandles/SysListHandle.h"
#include "TLorentzVector.h"
#include <string>
#include <iostream>
#include "TTree.h"
#include "TH1F.h"
#include "TH2D.h"
#include "TEfficiency.h"

namespace TMVA{
  class Reader;
}

class MyxAODAnalysis : public EL::AnaAlgorithm
{
  /// private:
  ///CP::SysListHandle m_systematicsList {this};

  /// \brief the muon collection we run on
  ///CP::SysReadHandle<xAOD::Jet> m_jetHandle {
  ///  this, "jets", "AnalysisMuons_%SYS%", "the muon collection to run on"};

  /// CP::SysReadHandle<xAOD::TruthParticle> m_TruthParticleHandle {
  ///  this, "truthParticle", "AnalysisMuons_%SYS%", "the muon collection to run on"};

public:
   xAOD::TEvent *m_event;  //!

public:
   std::string method; //!
   TTree *myTree; //!
   TMVA::Reader *m_mvaReader; //!
   int m_eventCounter; //!
   TH1F *h_doSig_fromB; //!

   // this is a standard constructor
   MyxAODAnalysis (const std::string& name, ISvcLocator* pSvcLocator);
   
   // these are the functions inherited from Algorithm
   virtual StatusCode initialize () override;
   ////virtual StatusCode histInitialize ();
   virtual StatusCode execute () override;
   virtual StatusCode finalize () override;

protected:

  void   getNeutrinoPz  (double MET, double MET_phi, const TLorentzVector &lepVec, double &nu_pz1, double &nu_pz2);
  double getNeutrinoPz  (double MET, double MET_phi, const TLorentzVector &lepVec, bool min=true); 
  StatusCode Reconstruct_ChargedHiggs();
};

#endif

#!/usr/bin/env python

# Read the submission directory as a command line argument. You can
# extend the list of arguments with your private ones later on.
import optparse
parser = optparse.OptionParser()
parser.add_option( '-s', '--submission-dir', dest = 'submission_dir',
                   action = 'store', type = 'string', default = 'submitDir',
                   help = 'Submission directory for EventLoop' )
( options, args ) = parser.parse_args()

# Set up (Py)ROOT.
import ROOT
ROOT.xAOD.Init().ignore()

# Set up the sample handler object. See comments from the C++ macro
# for the details about these lines.
import os
sh = ROOT.SH.SampleHandler()
sh.setMetaString( 'nc_tree', 'CollectionTree' )
inputFilePath      = '/ptmp/mpp/dduda/Samples/DxAODs/Truth/mc15_13TeV.344886.aMcAtNloPythia8EvtGen_A14NNPDF23LO_Hplus4FS_H600_Wh.evgen.EVNT.e5864/'
ROOT.SH.ScanDir().filePattern( '*.root*' ).scan( sh, inputFilePath )

# Create an EventLoop job.
job = ROOT.EL.Job()
job.sampleHandler( sh )

# Create the algorithm's configuration. Note that we'll be able to add
# algorithm property settings here later on.

from AnaAlgorithm.AnaAlgorithmConfig import AnaAlgorithmConfig
config = AnaAlgorithmConfig( 'MyxAODAnalysis/AnalysisAlg' )  ### HbbTagging/
job.algsAdd( config )

# Run the job using the direct driver.
driver = ROOT.EL.DirectDriver()
driver.submit( job, options.submission_dir )
